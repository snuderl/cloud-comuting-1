/**********************************
 * FILE NAME: MP1Node.cpp
 *
 * DESCRIPTION: Membership protocol run by this Node.
 * 				Definition of MP1Node class functions.
 **********************************/

#include "MP1Node.h"
#include <iostream>
#include <vector>

using namespace std;

/*
 * Note: You can change/add any functions in MP1Node.{h,cpp}
 */

/**
 * Overloaded Constructor of the MP1Node class
 * You can add new members to the class if you think it
 * is necessary for your logic to work
 */

const int LIST_SIZE = sizeof(NodeList);


int getId(Address* addr){
    int id = 0;
    memcpy(&id, &addr[0], sizeof(int));
    return id;
}

MemberListEntry mle(Address* addr, long heartbeat, long timestamp) {
    int id;
    short port;
    memcpy(&id, &addr->addr[0], sizeof(int));
    memcpy(&port, &addr->addr[4], sizeof(short));
    return MemberListEntry(id, port, heartbeat, timestamp);
}

void getAddress(MemberListEntry* entry, Address* addr){
    memcpy(&addr->addr[0], &entry->id, sizeof(int));
    memcpy(&addr->addr[4], &entry->port, sizeof(short));


    //cout << entry->id << " " << entry->port <<  endl;
    //cout << addr->getAddress() << endl;
}


MP1Node::MP1Node(Member *member, Params *params, EmulNet *emul, Log *log, Address *address) {
	for( int i = 0; i < 6; i++ ) {
		NULLADDR[i] = 0;
	}
	this->memberNode = member;
	this->emulNet = emul;
	this->log = log;
	this->par = params;
	this->memberNode->addr = *address;
}

/**
 * Destructor of the MP1Node class
 */
MP1Node::~MP1Node() {}

/**
 * FUNCTION NAME: recvLoop
 *
 * DESCRIPTION: This function receives message from the network and pushes into the queue
 * 				This function is called by a node to receive messages currently waiting for it
 */


int MP1Node::recvLoop() {
    if ( memberNode->bFailed ) {
    	return false;
    }
    else {
    	return emulNet->ENrecv(&(memberNode->addr), enqueueWrapper, NULL, 1, &(memberNode->mp1q));
    }
}

/**
 * FUNCTION NAME: enqueueWrapper
 *
 * DESCRIPTION: Enqueue the message from Emulnet into the queue
 */
int MP1Node::enqueueWrapper(void *env, char *buff, int size) {
	Queue q;
	return q.enqueue((queue<q_elt> *)env, (void *)buff, size);
}

/**
 * FUNCTION NAME: nodeStart
 *
 * DESCRIPTION: This function bootstraps the node
 * 				All initializations routines for a member.
 * 				Called by the application layer.
 */
void MP1Node::nodeStart(char *servaddrstr, short servport) {
    Address joinaddr;
    joinaddr = getJoinAddress();

    // Self booting routines
    if( initThisNode(&joinaddr) == -1 ) {
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "init_thisnode failed. Exit.");
#endif
        exit(1);
    }

    if( !introduceSelfToGroup(&joinaddr) ) {
        finishUpThisNode();
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Unable to join self to group. Exiting.");
#endif
        exit(1);
    }

    return;
}

void MP1Node::mergeNodeLists(NodeList* list){
    for(int i = 0; i < list->currentSize; i++){
        MemberListEntry entry = list->nodes[i];

        bool found = false;
        for(int y = 0; y < memberNode->memberList.size(); y++){
            auto localNode = memberNode->memberList[y];
            if(localNode.id == entry.id){
                found = true;
                if(entry.heartbeat > localNode.heartbeat){
                    memberNode->memberList[y].heartbeat = entry.heartbeat;
                    memberNode->memberList[y].timestamp = par->getcurrtime();
                }
                break;
            }
        }

        if(!found){
            Address addr;

            entry.timestamp = par->getcurrtime();
            memberNode->memberList.push_back(entry);

            // Hack for nodes that are using different ports
            getAddress(&entry, &addr);
            log->logNodeAdd(&memberNode->addr, &addr);
        }
    }
}

/**
 * FUNCTION NAME: initThisNode
 *
 * DESCRIPTION: Find out who I am and start up
 */
int MP1Node::initThisNode(Address *joinaddr) {
    // cout << sizeof(Address) << endl;
    // cout << sizeof(MemberListEntry) << endl;
    // cout << sizeof(NodeList) << endl;
	/*
	 * This function is partially implemented and may require changes
	 */
	int id = *(int*)(&memberNode->addr.addr);
	int port = *(short*)(&memberNode->addr.addr[4]);

	memberNode->bFailed = false;
	memberNode->inited = true;
	memberNode->inGroup = false;
    // node is up!
	memberNode->nnb = 0;
	memberNode->heartbeat = 0;
	memberNode->pingCounter = TFAIL;
	memberNode->timeOutCounter = -1;
    initMemberListTable(memberNode);

    return 0;
}


/**
 * FUNCTION NAME: introduceSelfToGroup
 *
 * DESCRIPTION: Join the distributed system
 */
int MP1Node::introduceSelfToGroup(Address *joinaddr) {
	MessageHdr *msg;
#ifdef DEBUGLOG
    static char s[1024];
#endif

    if ( 0 == memcmp((char *)&(memberNode->addr.addr), (char *)&(joinaddr->addr), sizeof(memberNode->addr.addr))) {
        // I am the group booter (first process to join the group). Boot up the group
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Starting up group...");
#endif
        memberNode->inGroup = true;
        long hb =  memberNode->heartbeat;
        long t = par->getcurrtime();
        MemberListEntry entry = mle(&memberNode->addr, hb, t);
        getMemberNode()->memberList.push_back(entry);
        log->logNodeAdd(&memberNode->addr, &memberNode->addr);
    }
    else {
        size_t msgsize = sizeof(MessageHdr) + sizeof(joinaddr->addr) + sizeof(long) + 10;
        msg = (MessageHdr *) malloc(msgsize * sizeof(char));

        // create JOINREQ message: format of data is {struct Address myaddr}
        msg->msgType = JOINREQ;
        memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
        memcpy((char *)(msg+1) + 1 + sizeof(memberNode->addr.addr), &memberNode->heartbeat, sizeof(long));

#ifdef DEBUGLOG
        sprintf(s, "Trying to join...");
        log->LOG(&memberNode->addr, s);
#endif

        // send JOINREQ message to introducer member
        emulNet->ENsend(&memberNode->addr, joinaddr, (char *)msg, msgsize);

        free(msg);
    }

    return 1;

}

/**
 * FUNCTION NAME: finishUpThisNode
 *
 * DESCRIPTION: Wind up this node and clean up state
 */
int MP1Node::finishUpThisNode(){
   /*
    * Your code goes here
    */
    return 0;
}

/**
 * FUNCTION NAME: nodeLoop
 *
 * DESCRIPTION: Executed periodically at each member
 * 				Check your messages in queue and perform membership protocol duties
 */
void MP1Node::nodeLoop() {
    if (memberNode->bFailed) {
    	return;
    }

    // Check my messages
    checkMessages();

    // Wait until you're in the group...
    if( !memberNode->inGroup ) {
    	return;
    }

    // ...then jump in and share your responsibilites!
    nodeLoopOps();

    return;
}

/**
 * FUNCTION NAME: checkMessages
 *
 * DESCRIPTION: Check messages in the queue and call the respective message handler
 */
void MP1Node::checkMessages() {
    void *ptr;
    int size;

    // Pop waiting messages from memberNode's mp1q
    while ( !memberNode->mp1q.empty() ) {
    	ptr = memberNode->mp1q.front().elt;
    	size = memberNode->mp1q.front().size;
    	memberNode->mp1q.pop();
    	recvCallBack((void *)memberNode, (char *)ptr, size);
    }
    return;
}

NodeList MP1Node::createPartialNodeList(){
    NodeList list;
    auto vector = getMemberNode()->memberList;
    for(unsigned int i = 0; i < vector.size(); i++){

    }
    return list;
}

/**
 * FUNCTION NAME: recvCallBack
 *
 * DESCRIPTION: Message handler for different message types
 */
bool MP1Node::recvCallBack(void *env, char *data, int size ) {
	/*
	 * Your code goes here
	 */
     //log->LOG(&memberNode->addr, "Reciving message.1");
     MessageHdr* hdr = (MessageHdr*)data;


     if(hdr->msgType == JOINREQ){
        //log->LOG(&memberNode->addr, "JOINREQ recieved");

        Address* nodeAddress = (Address*)(hdr+1);
        long heartbeat = *(long*)(((hdr+1) + 1 + sizeof(memberNode->addr.addr)));

        MemberListEntry entry = mle(nodeAddress, heartbeat, par->getcurrtime());
        NodeList list;
        list.currentSize = 1;
        list.nodes[0] = entry;
        mergeNodeLists(&list);

        sendMembershipList(nodeAddress, JOINREP);

     }
     if(hdr->msgType == JOINREP){
        //log->LOG(&memberNode->addr, "JOINREP recieved");
        nodeList = (NodeList*)(hdr+1);
        mergeNodeLists(nodeList);

        memberNode->inGroup = true;

     }
     if(hdr->msgType == PINGREQ){
        //log->LOG(&memberNode->addr, "PINGREQ recieved");
        Address* nodeAddress = (Address*)(hdr+1);
        size_t msgsize = sizeof(MessageHdr) + sizeof(Address) + sizeof(long) + 10;
        MessageHdr* msg = (MessageHdr *) malloc(msgsize * sizeof(char));
        msg->msgType = PINGREP;
        memcpy((char *)(msg+1), &memberNode->addr, sizeof(Address));
        memberNode->heartbeat++;
        memcpy((char *)(msg+1) + 1 + sizeof(Address), &memberNode->heartbeat, sizeof(long));
        emulNet->ENsend(&memberNode->addr, nodeAddress, (char *)msg, msgsize);
     }
     if(hdr->msgType == PINGREP){
        Address* nodeAddress = (Address*)(hdr+1);
        long heartbeat = *(long*)(hdr+1 + 1 +sizeof(Address));

        for(unsigned int i = 0; i < memberNode->memberList.size();i++){
            if(memberNode->memberList[i].id == getId(nodeAddress)){
                memberNode->memberList[i].heartbeat = heartbeat;
                memberNode->memberList[i].timestamp = par->getcurrtime();
                break;
            }
        }
     }
     if(hdr->msgType==MEMBERSHIP){
        nodeList = (NodeList*)(hdr+1);
        mergeNodeLists(nodeList);
     }
     // send JOINREQ message to introducer member
     return false;
}



void MP1Node::sendMembershipList(Address* target, MsgTypes type){
    auto vector = getMemberNode()->memberList;

    NodeList list;
    int c = 0;
    for(unsigned int i = 0; i < vector.size(); i++){
        if(shouldPing(vector[i])){
            list.nodes[c++] = vector[i];
        }
    }
    list.currentSize = c;


    size_t msgsize = sizeof(MessageHdr) + sizeof(NodeList) + 10;
    MessageHdr* msg = (MessageHdr *) malloc(msgsize * sizeof(char));

    // create JOINREQ message: format of data is {struct Address myaddr}
    msg->msgType = type;
    memcpy((char *)(msg+1), &list, sizeof(NodeList));
    emulNet->ENsend(&memberNode->addr, target, (char *)msg, msgsize);
}



bool MP1Node::shouldPing(MemberListEntry entry){
    return (par->getcurrtime() - entry.timestamp) < 5;
}

bool MP1Node::shouldRemove(MemberListEntry entry){
    return (par->getcurrtime() - entry.timestamp) > 40;
}

/**
 * FUNCTION NAME: nodeLoopOps
 *
 * DESCRIPTION: Check if any node hasn't responded within a timeout period and then delete
 *              the nodes
 *              Propagate your membership list
 */
void MP1Node::nodeLoopOps() {

    Address addr;

    vector<MemberListEntry> newVector;

    for(auto entry : memberNode->memberList){
        if(shouldRemove(entry)){
            getAddress(&entry, &addr);
            log->logNodeRemove(&memberNode->addr, &addr);
        }
        else{
            newVector.push_back(entry);
        }
    }

    memberNode->memberList = newVector;
    for(auto node : newVector){
        if(shouldPing(node)){
            getAddress(&node, &addr);

            size_t msgsize = sizeof(MessageHdr) + sizeof(Address) + 10;
            MessageHdr* msg = (MessageHdr *) malloc(msgsize * sizeof(char));

            // create JOINREQ message: format of data is {struct Address myaddr}
            msg->msgType = PINGREQ;
            memcpy((char *)(msg+1), &memberNode->addr, sizeof(Address));
            emulNet->ENsend(&memberNode->addr, &addr, (char *)msg, msgsize);
            //getMemberNode()->memberList->

            sendMembershipList(&addr, MEMBERSHIP);
        }
    }
}

/**
 * FUNCTION NAME: isNullAddress
 *
 * DESCRIPTION: Function checks if the address is NULL
 */
int MP1Node::isNullAddress(Address *addr) {
	return (memcmp(addr->addr, NULLADDR, 6) == 0 ? 1 : 0);
}

/**
 * FUNCTION NAME: getJoinAddress
 *
 * DESCRIPTION: Returns the Address of the coordinator
 */
Address MP1Node::getJoinAddress() {
    Address joinaddr;

    memset(&joinaddr, 0, sizeof(Address));
    *(int *)(&joinaddr.addr) = 1;
    *(short *)(&joinaddr.addr[4]) = 0;

    return joinaddr;
}

/**
 * FUNCTION NAME: initMemberListTable
 *
 * DESCRIPTION: Initialize the membership list
 */
void MP1Node::initMemberListTable(Member *memberNode) {
	memberNode->memberList.clear();
}

/**
 * FUNCTION NAME: printAddress
 *
 * DESCRIPTION: Print the Address
 */
void MP1Node::printAddress(Address *addr)
{
    printf("%d.%d.%d.%d:%d \n",  addr->addr[0],addr->addr[1],addr->addr[2],
                                                       addr->addr[3], *(short*)&addr->addr[4]) ;    
}
